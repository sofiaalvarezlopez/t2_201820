package test;

import java.util.Date;

import junit.framework.TestCase;
import model.data_structures.Node;
import model.vo.VOTrip;

public class NodeTest extends TestCase
{
	Node<VOTrip> firstNode;
	
	private void setUpScenario1()
	{
		VOTrip trip = new VOTrip(123, new Date(), new Date(), 321, 567.0, 654, "FromStation", 456, "ToStation", "Consumer", "Female", 2000);
		firstNode = new Node<VOTrip>(trip);
		VOTrip trip1 = new VOTrip(1234, new Date(), new Date(), 321012, 5678.0, 65432, "FromStation", 45678, "ToStation", "Consumer", "Female", 2000);
		firstNode.changePrevious(new Node<VOTrip>(trip1));
		VOTrip trip2 = new VOTrip(12345678, new Date(), new Date(), 321012456, 5678.0, 65432, "FromStation", 45678, "ToStation", "Consumer", "Female", 2000);
		firstNode.changeNext(new Node<VOTrip>(trip2));
	}
	
	public void testGetElement()
	{
		setUpScenario1();
		assertTrue("The elements should be the same", firstNode.getPrevious().getElement().getTripId() == 1234);
		assertTrue("The elements should be the same", firstNode.getElement().getTripId() == 123);
		assertTrue("The elements should be the same", firstNode.getNext().getElement().getTripId() == 12345678);
	}
	
	public void testGetNext()
	{
		setUpScenario1();
		assertTrue("The elements of the Nodes should be the same", firstNode.getNext().getElement().getTripId() == 12345678);
	}
	
	public void testGetPrevious()
	{
		setUpScenario1();
		assertTrue("The Nodes should be the same", firstNode.getPrevious().getElement().getTripId() == 1234);
	}
	
	public void testChangeElement()
	{
		setUpScenario1();
		VOTrip trip = new VOTrip(280600, new Date(), new Date(), 321, 567.0, 654, "FromStation", 456, "ToStation", "Consumer", "Female", 2000);
		assertTrue("The elements should be the same", firstNode.changeElement(trip).getTripId() == 280600);
	}
	
	public void testChangeNextAndPrevious()
	{
		setUpScenario1();
		VOTrip trip = new VOTrip(2000, new Date(), new Date(), 321, 567.0, 654, "FromStation", 456, "ToStation", "Consumer", "Female", 2000);
		firstNode.changePrevious(new Node<VOTrip>(trip));
		VOTrip trip1 = new VOTrip(1994, new Date(), new Date(), 321, 567.0, 654, "FromStation", 456, "ToStation", "Milo", "Male", 1994);
		firstNode.changeNext(new Node<VOTrip>(trip1));
		assertTrue("The nodes should be the same", firstNode.getPrevious().getElement().getTripId() == 2000);
		assertTrue("The nodes should be the same", firstNode.getNext().getElement().getTripId() == 1994);
	}

}
