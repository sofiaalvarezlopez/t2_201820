package test;

import java.util.Date;

import junit.framework.TestCase;
import model.data_structures.Iter;
import model.data_structures.Node;
import model.vo.VOTrip;

public class IterTest extends TestCase
{
	private Iter<VOTrip> iterator;
	
	private void setUpScenario1()
	{
		VOTrip trip = new VOTrip(123, new Date(), new Date(), 321, 567.0, 654, "FromStation", 456, "ToStation", "Consumer", "Female", 2000);
		Node<VOTrip> node = new Node<VOTrip>(trip);
		iterator = new Iter<VOTrip>(node);
	}
	
	private void setUpScenario2()
	{
		VOTrip trip = new VOTrip(123, new Date(), new Date(), 321, 567.0, 654, "FromStation", 456, "ToStation", "Consumer", "Female", 2000);
		Node<VOTrip> node = new Node<VOTrip>(trip);
		VOTrip trip1 = new VOTrip(1234, new Date(), new Date(), 3210, 789.0, 9876, "FromStation1", 456, "ToStation1", "Consumer", "Female", 2001);
		Node<VOTrip> node1 = new Node<VOTrip>(trip1);
		VOTrip trip2 = new VOTrip(1234, new Date(), new Date(), 3210, 789.0, 9876, "FromStation1", 456, "ToStation1", "Consumer", "Female", 2001);
		Node<VOTrip> node2 = new Node<VOTrip>(trip2);
		VOTrip trip3 = new VOTrip(1234, new Date(), new Date(), 3210, 789.0, 9876, "FromStation1", 456, "ToStation1", "Consumer", "Female", 2001);
		Node<VOTrip> node3 = new Node<VOTrip>(trip3);
		VOTrip trip4 = new VOTrip(1234, new Date(), new Date(), 3210, 789.0, 9876, "FromStation1", 456, "ToStation1", "Consumer", "Female", 2001);
		Node<VOTrip> node4 = new Node<VOTrip>(trip4);

		iterator = new Iter<VOTrip>(node);
		node.changeNext(node1);
		node1.changePrevious(node);
		node1.changeNext(node2);
		node2.changePrevious(node1);
		node2.changeNext(node3);
		node3.changePrevious(node2);
		node3.changeNext(node4);
		node4.changePrevious(node3);
	}
	
	public void testHasNext()
	{
		setUpScenario1();
		assertFalse("There is not a next element", iterator.hasNext());

		setUpScenario2();
		assertTrue("There are next elements", iterator.hasNext());
	}
	
	public void testNext()
	{
		setUpScenario1();
		try
		{
			iterator.next();
			fail("There are no objects to iterate over");
		}
		catch (Exception e) 
		{
			//The next element has to be null
		}

		setUpScenario2();
		int count = 0;
		while(iterator.hasNext())
		{
			assertNotNull(iterator.next());
			count++;
		}
		assertEquals("The counter had to iterate over 4 nodes", count, 4);
	}
	
	public void testHasPrevious()
	{
		setUpScenario1();
		assertFalse("There is not a previous element", iterator.hasPrevious());
		
		setUpScenario2();
		iterator.next();
		assertTrue("There are elements to iterate over", iterator.hasPrevious());
		iterator.next();
		assertTrue("There are elements to iterate over", iterator.hasPrevious());

	}
	
	public void testPrevious()
	{
		setUpScenario1();
		try
		{
			iterator.previous();
			fail("There is not a previous element");
		}
		catch (Exception e) 
		{
			// It has to enter the exception.
		}
		
		setUpScenario2();
		int count = 0;
		iterator.next();
		iterator.next();
		iterator.next();
		iterator.next();
		while(iterator.hasPrevious())
		{
			assertNotNull(iterator.previous());
			count++;
		}
		assertEquals("The counter had to iterate over 4 nodes", count, 4);
	}
	
}
