package test;

import java.util.Date;

import junit.framework.TestCase;
import model.data_structures.LinkedList;
import model.data_structures.Node;
import model.vo.VOTrip;

public class LinkedListTest extends TestCase
{

	private LinkedList<VOTrip> list;

	private void setUpScenario1()
	{
		VOTrip trip = new VOTrip(2000, new Date(), new Date(), 321, 567.0, 654, "FromStation", 456, "ToStation", "Sofia", "Female", 2000);
		VOTrip trip1 = new VOTrip(1994, new Date(), new Date(), 321, 567.0, 654, "FromStation", 456, "ToStation", "Milo", "Male", 1994);
		list = new LinkedList<VOTrip>(new Node<VOTrip>(trip));
		list.addAtEnd(trip1);
	}
	
	private void setUpScenario2()
	{
		list = new LinkedList<VOTrip>();
	}
	
	public void testInitialState()
	{
		setUpScenario1();
		assertTrue("There must be two nodes.", list.getSize() == 2);
		
		setUpScenario2();
		assertTrue("There must be two nodes.", list.getSize() == 0);
	}
	
	public void testAddFirst()
	{
		setUpScenario1();
		VOTrip trip1 = new VOTrip(26111994, new Date(), new Date(), 321, 567.0, 654, "FromStation", 456, "ToStation", "Novio", "Male", 1994);
		list.addFirst(trip1);
		assertTrue("The elements should be the same.", list.getNode(0).getElement().getTripId() == trip1.getTripId());
	
		setUpScenario2();
		list.addFirst(trip1);
		assertNotNull("The elements should not be null", list.getNode(0));
		
		trip1 = null;
		try
		{
			list.addFirst(trip1);
			fail("Can not add a null element");
		}
		catch (Exception e) 
		{
			// It should catch the Exception.
		}
		

	}
	
	public void testAddLast()
	{
		setUpScenario1();
		VOTrip trip2 = new VOTrip(2611, new Date(), new Date(), 321, 567.0, 654, "FromStation", 456, "ToStation", "Milo", "Male", 1994);
		list.addAtEnd(trip2);
		assertTrue("The elements should be the same.", list.getNode(list.getSize() - 1).getElement().getTripId() == trip2.getTripId());
		
		setUpScenario2();
		list.addAtEnd(trip2);
		assertNotNull("The elements should not be null", list.getNode(0));
		
		trip2 = null;
		try
		{
			list.addAtEnd(trip2);
			fail("Can not add a null element");
		}
		catch (Exception e) 
		{
			// It should catch the Exception.
		}	
	}
	
	public void testAddAtK()
	{
		setUpScenario1();
		VOTrip trip3 = new VOTrip(28062000, new Date(), new Date(), 321, 567.0, 654, "FromStation", 456, "ToStation", "Sofi", "Male", 1994);
		list.addAtK(1, trip3);
		assertTrue("The IDs should be the same.", list.getNode(1).getElement().getTripId() == trip3.getTripId());
		
		setUpScenario2();
		list.addAtK(0, trip3);
		assertNotNull("The elements should not be null", list.getNode(0));
		
		trip3 = null;
		try
		{
			list.addAtK(2, trip3);
			fail("Can not add a null element");
		}
		catch (Exception e) 
		{
			// It should catch the Exception.
		}
	}
	
	public void testDelete()
	{
		setUpScenario1();
		list.delete(list.getElement(0));
		assertTrue("", list.getElement(0).getTripId() == 1994);
		
		setUpScenario1();
		list.delete(list.getElement(1));
		try
		{
			list.getElement(1);
			fail("It should generate the Exception");
		}
		catch (Exception e) 
		{
			// It should generate the Exception.
		}
		
		setUpScenario2();
		try
		{
			list.delete(list.getElement(0));
			fail("It should generate the Exception");
		}
		catch (Exception e) 
		{
			// It should generate the Exception.
		}		
	}
	
	public void testDeleteAtK()
	{
		setUpScenario1();
		VOTrip trip = list.deleteAtK(0);
		assertTrue("", trip.getTripId() == 2000);
		
		setUpScenario1();
		list.deleteAtK(1);
		try
		{
			list.getElement(1);
			fail("It should generate the Exception");
		}
		catch (Exception e) 
		{
			// It should generate the Exception.
		}	
	}
}
