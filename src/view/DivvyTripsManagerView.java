package view;

import java.io.FileNotFoundException;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.DoublyLinkedList;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:
				try
				{
					Controller.loadStations();
				}
				catch (FileNotFoundException e) 
				{
					System.out.println(e.getMessage());
				}
				catch(Exception e1)
				{
					System.out.println(e1.getMessage());
				}
				break;

			case 2:
				try
				{
					Controller.loadTrips();
				}
				catch (FileNotFoundException e) 
				{
					System.out.println(e.getMessage());
				}
				catch(Exception e1)
				{
					System.out.println(e1.getMessage());
				}
				break;

			case 3:
				System.out.println("Ingrese el genero:");
				String gender = sc.next();
				DoublyLinkedList<VOTrip> bykeTripsList = Controller.getTripsOfGender (gender);
				System.out.println("Se encontraron "+ bykeTripsList.getSize() + " elementos");
				break;

			case 4:
				System.out.println("Ingrese el identificador de la estacion:");
				int stationId = Integer.parseInt(sc.next());
				DoublyLinkedList <VOTrip> bykeTripsList2 = Controller.getTripsToStation(stationId);
				System.out.println("Se encontraron " + bykeTripsList2.getSize() + " elementos");
				break;

			case 5:	
				fin=true;
				sc.close();
				break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 2----------------------");
		System.out.println("1. Cree una nueva coleccion de estaciones");
		System.out.println("2. Cree una nueva coleccion de viajes");
		System.out.println("3. Dar listado de viajes realizados dado un genero");
		System.out.println("4. Dar listado de viajes que finalizan en una estacion de bicicletas especifica");
		System.out.println("5. Salir");
		System.out.println("Digite el numero de opcion para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}
}
