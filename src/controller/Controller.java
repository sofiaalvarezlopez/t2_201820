package controller;

import api.IDivvyTripsManager;

import java.io.File;
import java.io.FileNotFoundException;

import model.data_structures.DoublyLinkedList;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller 
{
	private final static String STATIONS_FILE = "." + File.separator + "src" + File.separator + "data" + File.separator + "Divvy_Stations_2017_Q3Q4.csv";
	private final static String TRIPS_FILE = "." + File.separator + "src" + File.separator + "data" + File.separator + "Divvy_Trips_2017_Q4.csv";

	/**
	 * Reference to the services manager
	 */
	private static IDivvyTripsManager  manager = new DivvyTripsManager();

	public static void loadStations() throws FileNotFoundException, Exception 
	{
		manager.loadStations(STATIONS_FILE);
	}

	public static void loadTrips() throws FileNotFoundException, Exception 
	{
		manager.loadTrips(TRIPS_FILE);
	}

	public static DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		return manager.getTripsOfGender(gender);
	}

	public static DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		return manager.getTripsToStation(stationID);
	}
}
