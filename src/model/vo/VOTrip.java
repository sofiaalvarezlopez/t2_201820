package model.vo;

import java.util.Date;

/**
 * Representation of a Trip object
 */
public class VOTrip 
{
	private int id;
	private Date startTime;
	private Date endTime;
	private int bikeId;
	private double tripSeconds;
	private int getFromStationId;
	private String getFromStationName;
	private int getToStationId;
	private String getToStationName;
	private String usertype;
	private String gender;
	private int birthYear;

	public VOTrip(int pId, Date pStart, Date pEnd, int pBikeId, double pTripSeconds, int pFromStationId,
			String pFromStationName, int pToStationId, String pToStationName, String pUsertype, 
			String pGender, int pBirthYear)
	{
		id = pId;
		startTime = pStart;
		endTime = pEnd;
		bikeId = pBikeId;
		tripSeconds = pTripSeconds;
		getFromStationId = pFromStationId;
		getFromStationName = pFromStationName;
		getToStationId = pToStationId;
		getToStationName = pToStationName;
		usertype = pUsertype;
		gender = pGender;
		birthYear = pBirthYear;		
	}

	/**
	 * @return id - Trip_id
	 */
	public int getTripId() {
		// TODO Auto-generated method stub
		return id;
	}	

	public Date getStartTime()
	{
		return startTime;	
	}

	public Date getEndTime()
	{
		return endTime;	
	}

	public int getBikeId()
	{
		return bikeId;
	}

	public double getTripSeconds()
	{
		return tripSeconds;
	}

	public int getFromStationId()
	{
		return getFromStationId;
	}

	public String getFromStationName()
	{
		return getFromStationName;
	}

	public int getToStationId()
	{
		return getToStationId;
	}

	public String getToStationName()
	{
		return getToStationName;
	}

	public String getUsertype()
	{
		return usertype;
	}

	public String getGender()
	{
		return gender;
	}

	public int getBirthYear()
	{
		return birthYear;
	}

}
