package model.vo;

import java.util.Date;

/**
 * Representation of a station object
 */
public class VOStation 
{
	private int id;
	private String name;
	private String city;
	private double latitude;
	private double longitude;
	private int dpCapacity;
	private Date date;
	
	public VOStation(int pId, String pName, String pCity, double pLat, double pLong, int pDpCapacity, Date pDate)
	{
		id = pId;
		name = pName;
		city = pCity;
		latitude = pLat;
		longitude = pLong;
		dpCapacity = pDpCapacity;
		date = pDate;
	}
	
	public int getId()
	{
		return id;
	}
	
	public String getStationName()
	{
		return name;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public double getLatitude()
	{
		return latitude;
	}
	
	public double getLongitude()
	{
		return longitude;
	}
	
	public int getDpCapacity()
	{
		return dpCapacity;
	}
	
	public Date getDate()
	{
		return date;
	}
		
}
