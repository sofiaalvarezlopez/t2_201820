package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.sun.media.sound.InvalidFormatException;
import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.Iter;
import model.data_structures.LinkedList;

public class DivvyTripsManager implements IDivvyTripsManager 
{
	private LinkedList<VOTrip> VOTrips;
	private LinkedList<VOStation> VOStations;

	public DivvyTripsManager()
	{
		VOTrips = new LinkedList<VOTrip>();
		VOStations = new LinkedList<VOStation>();
	}


	public void loadStations (String stationsFile) throws FileNotFoundException, Exception
	{
		File f = new File(stationsFile);
		BufferedReader br= null;
		try
		{
			if(stationsFile.endsWith(".csv"))
			{
				FileReader fr = new FileReader(f);
				br = new BufferedReader(fr);
				String line = br.readLine();

				if(line == null)
				{
					throw new InvalidFormatException("The file is empty");
				}	
				//Starts reading the stations.
				line = br.readLine();
				while(line != null)
				{
					String[] stationInfo = line.split(",");
					int id = Integer.parseInt(stationInfo[0]);
					String name = stationInfo[1];
					String city = stationInfo[2];
					double latitude = Double.parseDouble(stationInfo[3]);
					double longitude = Double.parseDouble(stationInfo[4]);
					int dpCapacity = Integer.parseInt(stationInfo[5]);
					Date date = new SimpleDateFormat("MM/dd/yyyy kk:mm").parse(stationInfo[6]);
					VOStation station = new VOStation(id, name, city, latitude, longitude, dpCapacity, date);
					VOStations.addFirst(station);	
					line = br.readLine();
				}
			}
			else
			{
				throw new FileNotFoundException("Invalid format for the file");
			}
		}
		catch (IOException e) 
		{
			throw new Exception(" The file could not be read: " + e.getMessage());
		}
		finally
		{
			try
			{
				br.close();
			}
			catch(IOException e1)
			{
				throw new Exception("Could not close the Buffered Reader"); 
			}
		}
	}

	public void loadTrips (String tripsFile) throws FileNotFoundException, Exception 
	{
		File f = new File(tripsFile);
		FileReader fr = new FileReader(f);
		CSVReader csvReader = new CSVReaderBuilder(fr).withSkipLines(1).build();
		String[] nextLine;
		while((nextLine = csvReader.readNext()) != null)
		{
			int tripId = Integer.parseInt(nextLine[0]);
			Date start = new SimpleDateFormat("MM/DD/yyyy kk:mm").parse(nextLine[1]);
			Date end = new SimpleDateFormat("MM/DD/yyyy kk:mm").parse(nextLine[2]);
			int bikeId = Integer.parseInt(nextLine[3]);
			double tripDuration = Double.parseDouble(nextLine[4]);
			int fromStationId = Integer.parseInt(nextLine[5]);
			String fromStationName = nextLine[6];
			int toStationId = Integer.parseInt(nextLine[7]);
			String toStationName = nextLine[8];
			String usertype = nextLine[9];
			String gender;
			int birthYear;
			try
			{
				gender = nextLine[10];
				birthYear = Integer.parseInt(nextLine[11]);
			}
			//It catches the Exception if the gender, birth year or both are empty Strings.
			catch(Exception e)
			{
				gender = "";
				birthYear = 0;
			}
			VOTrip trip = new VOTrip(tripId, start, end, bikeId, tripDuration, fromStationId, fromStationName, toStationId, toStationName, usertype, gender, birthYear);
			VOTrips.addFirst(trip);
		}
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) 
	{
		DoublyLinkedList<VOTrip> linkedList = new LinkedList<VOTrip>();
		Iter<VOTrip> iter = (Iter<VOTrip>) VOTrips.iterator();
		while(iter.hasNext())
		{
			//System.out.println(iter.next().getGender());
			VOTrip item = iter.next();
			String trip = item.getGender();
			if(trip.equals(gender))
			{
				linkedList.addFirst(item);
			}
		}


		// TODO Auto-generated method stub
		return linkedList;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) 
	{
		DoublyLinkedList<VOTrip> linkedList = new LinkedList<VOTrip>();
		Iter<VOTrip> iter = (Iter<VOTrip>) VOTrips.iterator();
		while(iter.hasNext())
		{
			VOTrip item = iter.next();
			int trip = item.getToStationId();
			if(trip == stationID)
			{
				linkedList.addFirst(item);
			}
		}
		// TODO Auto-generated method stub
		return linkedList;
	}	


}
