package model.data_structures;

import java.util.ListIterator;


public class LinkedList<E> implements DoublyLinkedList<E> 
{
	private int amountOfNodes;
	private Node<E> firstNode;
	private Node<E> nextNode;

	/**
	 * Creates an empty list.
	 * <b>post:</b> The first node has been initialized in null.
	 */
	public LinkedList()
	{
		firstNode = null;
		nextNode = null;
		amountOfNodes = 0;
	}

	/**
	 * Creates a new Linked List with the node in the parameter.
	 * @param firstNode First Node of the Linked List.
	 * @throws NullPointerException if the parameter is null
	 * <b>post:</b> The amount of nodes has been set to 1.
	 */
	public LinkedList(Node<E> firstNode)
	{
		if(firstNode == null)
		{
			throw new NullPointerException("The element received is null");
		}
		this.firstNode = firstNode;
		amountOfNodes = 1;
	}

	public int getAmountOfNodes()
	{
		return amountOfNodes;
	}

	@Override
	public ListIterator<E> iterator() 
	{
		// TODO Auto-generated method stub
		return new Iter<E>(firstNode);
	}	

	public ListIterator<E> listIterator(int k) 
	{
		if(k< 0 || k >= getSize())
			throw new IndexOutOfBoundsException("The index is out of the bounds of the list");
		return new Iter<E>(getNode(k));
	}


	@Override
	public boolean addFirst(E element)
	{
		if(element == null)
		{
			throw new NullPointerException("Null element received");
		}
		else
		{
			if(firstNode == null)
			{
				firstNode = new Node<E>(element);
				amountOfNodes++;
				return true;
			}
			else
			{				
				Node<E> current = firstNode;
				Node<E> insertFirst = new Node<E>(element);
				insertFirst.changeNext(current);
				current.changePrevious(insertFirst);
				firstNode = insertFirst;
				nextNode = current;
				amountOfNodes++;
				return true;
			}
		}
	}

	@Override
	public boolean addAtEnd(E element) 
	{
		if(element == null)
		{
			throw new NullPointerException("Null element received");
		}
		else
		{
			if(firstNode == null)
			{
				firstNode = new Node<E>(element);
				amountOfNodes++;
				return true;
			}
			else
			{
				Node<E> current = firstNode;
				while(current.getNext() != null)
				{
					current = current.getNext();
				}
				Node<E> temp = new Node<E>(element);
				current.changeNext(temp);
				temp.changePrevious(current);
				amountOfNodes++;
				return true;
			}
		}
		// TODO Auto-generated method stub
	}

	@Override
	public void addAtK(int k, E element) 
	{
		if (element == null)
		{
			throw new NullPointerException("Se recibe un elemento nulo");
		}
		if(k < 0 || k > amountOfNodes)
		{
			throw new IndexOutOfBoundsException("Se está pidiendo el indice: " + k + " y el tamaño de la lista es de " + amountOfNodes);
		}
		Node<E> current = firstNode;
		Node<E> newNode = new Node<E>(element);
		
		if(firstNode == null && k == 0)
		{
			firstNode = new Node<E>(element);
			amountOfNodes++;
		}
		
		else if (k == 0)
		{
			newNode.changeNext(current);
			current.changePrevious(newNode);
			firstNode = newNode;
			amountOfNodes++;
		}
		else
		{
			for(int i = 0; i < k; i++)
			{
				current = current.getNext();
			}
			newNode.changePrevious(current.getPrevious());
			current.getPrevious().changeNext(newNode);
			newNode.changeNext(current);
			current.changePrevious(newNode);;
			amountOfNodes++;
		}
		// TODO Auto-generated method stub
	}

	@Override
	public E getElement(int k)
	{
		if(k < 0 || k > amountOfNodes)
		{
			throw new IndexOutOfBoundsException("The requested index: " + k + 
					" is out of the bounds of the list with " + amountOfNodes + " elements.");
		}
		else 
		{
			if(k == 0 && firstNode != null)
			{
				return firstNode.getElement();
			}
			else
			{
				Node<E> current = firstNode;
				if (current != null) 
				{
					for (int i = 0; i < k; i++)
					{
						current = current.getNext();
					}
					return current.getElement();
				}
				else
				{
					return null;
				}
			}

		}
		// TODO Auto-generated method stub
	}

	@Override
	public Node<E> getNode(int k) 
	{
		if(k < 0 || k > amountOfNodes)
		{
			throw new IndexOutOfBoundsException("The index " + k + " is out of the bounds of the list with " + 
					amountOfNodes + " elements.");
		}
		else if(k == 0 && firstNode != null)
		{
			return firstNode;
		}
		else
		{
			Node<E> currentNode = firstNode;
			int count = 0;
			while(currentNode != null && count < k)
			{
				currentNode = currentNode.getNext();
				count++;
			}
			return currentNode;
		}
		// TODO Auto-generated method stub
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return amountOfNodes;
	}

	@Override
	public boolean delete(Object o) throws NullPointerException 
	{
		boolean could = false;
		if(firstNode.getElement().equals(o))
		{
			firstNode = firstNode.getNext();
			amountOfNodes--;
			could = true;
		}
		else
		{
			Node<E> currentNode = firstNode;
			while(currentNode.getNext().getNext() != null && !could)
			{
				if(currentNode.getNext().getElement().equals(o))
				{
					Node<E> newNext = currentNode.getNext().getNext();
					nextNode = currentNode.getNext();
					nextNode.changePrevious(currentNode);
					currentNode.changeNext(currentNode.getNext().getNext());
					newNext.changePrevious(currentNode);
					currentNode.getNext().changeElement(null);
					amountOfNodes--;
					could = true;
				}
				currentNode = currentNode.getNext();
			}
			//In case the element to be erased is in the penultimate position of the list.
			if(currentNode.getElement().equals(o))
			{
				Node<E> newPrevious = currentNode.getPrevious();
				currentNode.getPrevious().changeNext(currentNode.getNext());
				currentNode.getNext().changePrevious(newPrevious);
				amountOfNodes--;
				could = true;
			}
			//In case the element to be erased is the last one in the list.
			else if(currentNode.getNext().getElement().equals(o))
			{
				currentNode.changeNext(null);
				amountOfNodes--;
				could = true;
			}

		}
		// TODO Auto-generated method stub
		return could;
	}

	@Override
	public E deleteAtK(int k) throws IndexOutOfBoundsException 
	{
		Node<E> delete;
		if(k < 0 || k > amountOfNodes)
		{
			throw new IndexOutOfBoundsException("The index " + k + " is out of the bounds "
					+ "of the list with " + amountOfNodes + " elements");
		}
		else if(k == 0 && firstNode != null)
		{
			delete = firstNode;
			firstNode = firstNode.getNext();
			amountOfNodes--;
			return delete.getElement();
		}
		else
		{
			Node<E> current = firstNode;
			int count = 0;
			while(count < k)
			{
				current = current.getNext();
				count++;
			}
			//If it is the last element of the list
			if(current.getNext() == null)
			{
				delete = current;
				current.getPrevious().changeNext(null);
				amountOfNodes--;
				return delete.getElement();
			}
			else
			{
				delete = current;
				Node<E> newPrevious = current.getPrevious();
				current.getPrevious().changeNext(current.getNext());
				current.getNext().changePrevious(newPrevious);
				return delete.getElement();
			}		
		}
		// TODO Auto-generated method stub
	}

	@Override
	public Node<E> next() 
	{
		return firstNode.getNext() != null ? firstNode.getNext() : null;
	}

	@Override
	public Node<E> previous() {
		// TODO Auto-generated method stub
		return firstNode.getPrevious() != null ? firstNode.getPrevious() : null;
	}

}
