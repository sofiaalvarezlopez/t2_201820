package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * addFirst, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface DoublyLinkedList<T> extends Iterable<T> 
{
	boolean addFirst(T element);
	
	boolean addAtEnd(T element);
	
	void addAtK(int k, T element);
	
	T getElement(int k);
		
	Node<T> getNode(int k);
	
	Integer getSize();
	
	boolean delete(Object o);
	
	T deleteAtK(int k);
	
	Node<T> next();
	
	Node<T> previous();

}
