package model.data_structures;

import java.util.ListIterator;

public class Iter <E> implements ListIterator<E>
{
	Node<E> previous;
	Node<E> current;

	public Iter(Node<E> firstNode)
	{
		current = firstNode;
		previous = null;
	}

	@Override
	public boolean hasNext() 
	{
		// TODO Auto-generated method stub
		return current.getNext() != null;
	}

	@Override
	public E next() {
		// TODO Auto-generated method stub
		current = current.getNext();
		return current.getElement() ;
	}

	@Override
	public boolean hasPrevious() 
	{
		// TODO Auto-generated method stub
		return current.getPrevious() != null;
	}

	@Override
	public E previous() {
		// TODO Auto-generated method stub
		current = current.getPrevious();
		return current.getElement();
	}

	//=======================================================
	// Unimplemented methods
	//=======================================================

	public int nextIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public int previousIndex() 
	{
		throw new UnsupportedOperationException();
	}

	public void remove() 
	{
		throw new UnsupportedOperationException();
	}

	public void set(E e) 
	{
		throw new UnsupportedOperationException();
	}

	public void add(E e) 
	{
		throw new UnsupportedOperationException();		
	}


}
