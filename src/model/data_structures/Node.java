package model.data_structures;

public class Node<E>
{
	private E element;
	private Node<E> next;
	private Node<E> previous;

	public Node(E element)
	{
		this.element = element;
	}

	public E getElement()
	{
		return element;
	}

	public Node<E> getNext()
	{
		return this.next;
	}

	public Node<E> getPrevious()
	{
		return this.previous;
	}

	public E changeElement(E newElement)
	{
		element = newElement;
		return element;
	}

	public void changeNext(Node<E> newNext)
	{
		next = newNext;
	}

	public void changePrevious(Node<E> newPrevious)
	{
		previous = newPrevious;
	}


}
